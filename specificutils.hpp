int getCount(){
    int count = 0;
    FILE *file;
    file = fopen(DB_Size,"r+");
		if (!file)
		{   
            char in;
			cout << "Unable to read file!\nIs this the First Entry ? (y/N) : ";
            cin >> in;
            if(in == 'y' || in == 'Y'){
                return 0;
            }
            return -2;
		}
        else{
            fread(&count, sizeof(int), 1, file);
            fclose(file);
        }
        //printf("%i", count);
    return  count;
}

void incCount(){
    int count = getCount() + 1;
    FILE *file;
    remove(DB_Size);
    file = fopen(DB_Size,"w+");
    if(!file){
        cout << "error";
        return;
    }else
    {
        fwrite(&count, sizeof(int), 1, file);
        fclose(file);
    }
    
}

void decCount(){
    int count = getCount() - 1;
    FILE *file;
    remove(DB_Size);
    file = fopen(DB_Size,"w+");
    if(!file){
        cout << "error";
        return;
    }else
    {
        fwrite(&count, sizeof(int), 1, file);
        fclose(file);
    }
    
}

int getID(){
    int id = getCount() + 1;
    if(id < 0){
        return -2;
    }
    /*FILE *file;
    file = fopen(DB_Size,"w+");
		if (!file)
		{
			cout << "Unable to read file!\n";
            return -2;
		}
        else{
            //cout << "ID : " << id << endl;
            fwrite(&id, sizeof(int), 1, file);
            fclose(file);
        }*/
        return id;
}

int overwriteSong(struct song in){
    FILE *file;
    file=fopen(DB,"w+");
		if (!file)
		{
			printf("Unable to write file!\n");
			return 1;
		}
        else{
            fseek(file, 0, SEEK_END);
            fwrite(&in, sizeof(struct song), 1, file);
            fclose(file);
            //cout << "Successfully writen !" << endl;
            //cout << "This entry NR.:" << EntryCount << endl;
            return 0;
        }
}

int writeSong(struct song in){
    FILE *file;
    file=fopen(DB,"app");
		if (!file)
		{
			printf("Unable to write file!\n");
			return 1;
		}
        else{
            fseek(file, 0, SEEK_END);
            fwrite(&in, sizeof(struct song), 1, file);
            fclose(file);
            incCount();
            cout << "Successfully writen !" << endl;
            //cout << "This entry NR.:" << EntryCount << endl;
            return 0;
        }
}

int writeSong_silent(struct song in){
    FILE *file;
    file=fopen(DB,"app");
		if (!file)
		{
			printf("Unable to write file!\n");
			return 1;
		}
        else{
            fseek(file, 0, SEEK_END);
            fwrite(&in, sizeof(struct song), 1, file);
            fclose(file);
            incCount();
            //cout << "Successfully writen !" << endl;
            //cout << "This entry NR.:" << EntryCount << endl;
            return 0;
        }
}


today currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y_%X", &tstruct);
    today temp;
    cpstr(buf, temp.da);
    return temp;
}
