Commands : 
	add - add a new entry

	remove {id} - delete song, specifed by ID
	
	list - list all existing entries
	
	play {id} - play song, specified by id (requires mpd123)
	
	exit - exit programm
	
	help - view this page (requires cat)
