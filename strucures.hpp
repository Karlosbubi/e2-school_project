struct date // 12 Bytes 
{
    int year;   //bytes 0-3
    int month;  //bytes 4-7
    int day;    //bytes 8-11
};

struct song // 262 Bytes
{ 
    int id;                     //argument 1  -> 0 //bytes 0-3
    int duration;               //argument 2  -> 1 //bytes 4-7
    int rating;                 //argument 3  -> 2 //bytes 8-11
    int times_played;           //argument 4  -> 3 //bytes 12-15
    char band[100];              //argument 5  -> 4 //bytes 16-35
    char title[100];             //argument 6  -> 5 //bytes 36-85
    char path[500];              //argument 7  -> 6 //bytes 86-185
    struct date release;       //argument 8  -> 7 //bytes 186-237 
    char last_played[100];    //argument 9  -> 8 //bytes 238-249
    char added[100];          //argument 10 -> 9 //bytes 250-261
};

struct today
{
    char da[100];
};

