void makebig(char* in){
    for(int i = 0; in[i] != '\0'; i++){
        if((in[i] >= 97) && (in[i] <= 122)){
            in[i] -= 32;
        }
    }
}

/*int strlen(char* in){
    int i = 0;
    while(in[i] != '\0'){
        i++;
    }
    return i;
}*/

void turn(char* in, char* out, int size){
    for(int i = 0; i <= size; i++){
        out[i] = in[size - i];
        //std::cout << out[i] << "," << in[i] << std::endl;
    }
}

void cpstr(char* in, char* out){
    for(int i = 0; in[i] != '\0'; i++){
        out[i] = in[i];
    }
}

int compare(char* uno, char* dos){
    for(int i = 0; (uno[i] != '\0') && (dos[i] != '\0'); i++){
        if(uno[i] != dos[i]){
            return 0;
        }
    }
    return 1;
}

int chartoint(char* in){
    int integer = 0;
    for(int i = 0; in[i] != '\0'; i++){
        if((in[i] <= '9') && (in[i] >= '0')){
            integer += in[i] - 48;
            integer *= 10;
        }
        else{
            return integer;
        }
    }
    integer /= 10;
    return integer;
}

float chartofloat(char* in){
    float integer = 0;
    float temp = 0;
    int i = 0;
    int a = 1;
    float s = 1;
    for(i = 0; in[i] != '\0'; i++){
        if((in[i] <= '9') && (in[i] >= '0')){
            integer += in[i] - 48;
            integer *= 10;
        }
        else if((in[i] == ',') || (in[i] == '.')){
            i++;
            break;
        }
        else{
            return integer;
        }
    }
    integer /= 10;
    for(int p = 0; in[i] != '\0'; i++){
        if((in[i] <= '9') && (in[i] >= '0')){
            s = 1;
            for(int j = 1; j <= a; j++){
                s *= 10;
            }
            temp = (in[i] - 48) / s;
            integer += temp;
            a++;
        }
        else{
            return integer;
        }
    }
    return integer;
}

void swapint(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}


int minint(int* in, int count){
    int temp = *in;
    for(int i = 0; i < count; i++){
        if(*(in + i) < temp){
            temp = *(in + i);
        }
    }
    return temp;
}

int maxint(int* in, int count){
    int temp = *in;
    for(int i = 0; i < count; i++){
        if(*(in + i) > temp){
            temp = *(in + i);
        }
    }
    return temp;
}

float minfloat(float* in, int count){
    float temp = *in;
    for(int i = 0; i < count; i++){
        if(*(in + i) < temp){
            temp = *(in + i);
        }
    }
    return temp;
}

float maxfloat(float* in, int count){
    float temp = *in;
    for(int i = 0; i < count; i++){
        if(*(in + i) > temp){
            temp = *(in + i);
        }
    }
    return temp;
}