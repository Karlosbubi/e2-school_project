#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <time.h>

using namespace std;

#define EntrySize sizeof(struct song)
#define EntryCount getCount()
//paths
#define DB "songs.kmp" // 'D'ata'B'ase
#define DB1 "songs1.kmp"
#define DB_Size "DB_Size.kmp"
#define help "/home/kurt/Documents/Schule/Informatik/db_Project_E2/help.txt"

#include "strucures.hpp"
#include "ownutils.hpp"
#include "specificutils.hpp"
#include "commands.hpp"


int main(){
    
    //init file
    FILE *file;

    //argumente auslesen
    string command = "";
    int id;
    //cout << sizeof(song);
    //cout << sizeof(size_t);
    while(1){
    command = "";
    cout << "\n>";
    cin >> command;

    //Ausführen
    if(!command.compare("add")){
       add();
    }
    else if(!command.compare("list")){
        list();
    }
    else if(!command.compare("search")){
        //search();
    }
    else if(!command.compare("remove")){
        id = 0;
        cin >> id;
        remove_song(id);
    }
    else if(!command.compare("play")){
        id = 0;
        cin >> id;
        play(id);
    }
    else if(!command.compare("help")){
        string cat = "cat ";
        cat += help;
        system(cat.c_str());
    }
    else if(!command.compare("exit")){
        return 0;
    }
    else{
        cout << "ERROR : Unknown action !\nTry 'help' to list all possible commands.";
        command = "";
    }
    }

    return 1;
}