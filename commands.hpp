int add(){
    struct song new_song;
    cout << "Title : ";
    cin >> new_song.title;
    
    new_song.id = getID();
    cout << "ID : " << new_song.id << endl;
    if(new_song.id < 0){
        cout << "an error occured processing the information\n";
        return -1;
    }

    cout << "Band : ";
    cin >> new_song.band;

    cout << "Path : ";
    cin >> new_song.path;

    cout << "Releas Date (DD MM YYYY) : ";
    cin >> new_song.release.day;
    cin >> new_song.release.month;
    cin >> new_song.release.year;

    cout << "Duration (in Sec.) : ";
    cin >> new_song.duration;

    cout << "Rating (0-5) : ";
    cin >> new_song.rating;
    if(new_song.rating > 5){
        new_song.rating = 5;
    }

    new_song.times_played = 0;
    cpstr(currentDateTime().da, new_song.added);
    cpstr("Never", new_song.last_played);

    return writeSong(new_song);
}

int remove_song(int id){
    song current_song[EntryCount];
    int j = 0;
    FILE *file;
    file = fopen(DB,"rb");
	if (!file)
		{
			cout << "Unable to read file!\n";
	}
    else{
        for(int i = 0; (i < EntryCount); i++){
            fread(&current_song[i - j], EntrySize, 1, file);
            if(current_song[i - j].id == id){ // overwrite the one (or more) to delete
                j++;
            }

            //cout << "id = " << current_song[i].id << " ID = " << id;
            current_song[i - j].id -= j; //set down IDs foe following
            //cout << " I = " << i << " J = " << j << " id = " << current_song[i-j].id << endl;
    }
        fclose(file);
    remove(DB);
    file = fopen(DB,"w+");
    for(int i = 0; i < (EntryCount - 1); i++){
        fwrite(&current_song[i], EntrySize, 1, file);
    }
    fclose(file);

    //reduce Counter
    decCount();
    }
}

int play(int id){

    string aplay = "mpg123 ";
    song current_song;

    FILE *file;
    file = fopen(DB,"rb");
		if (!file)
		{
			cout << "Unable to read file!\n";
            return -2;
		}
        else{
            fseek(file, EntrySize * (id - 1), SEEK_SET);
            fread(&current_song, EntrySize, 1, file);
            fclose(file);
        }

    if(id != current_song.id){
        cout << "Error finding song !";
        return -3;
    }


    aplay += current_song.path;
    system(aplay.c_str());

    current_song.times_played += 1;
    cpstr(currentDateTime().da, current_song.last_played);
    remove_song(id);
    current_song.id = getID();
    writeSong_silent(current_song);
    
    /*file = fopen(DB,"wb");
    if (!file)
		{
			cout << "Unable to read file!\n";
            return -2;
		}
        else{
            fseek(file, EntrySize * id, SEEK_SET);
            fwrite(&current_song, EntrySize, 1, file);
            fclose(file);
        }*/
    return 0;
}

void list(){
    song current_song;
    //int i = 0;
    FILE *file;
    file = fopen(DB,"rb");
		if (!file)
		{
			cout << "Unable to read file!\n";
		}
        else{
            for(int i = 0;(i < EntryCount); i++){
                fread(&current_song, EntrySize, 1, file);
                cout << "Title : " << current_song.title << endl;
                cout << "ID : " << current_song.id << endl;
                cout << "Band : " << current_song.band << endl;
                cout << "Times PLayed : " << current_song.times_played << endl;
                cout << "Last PLayed : " << current_song.last_played << endl;
                cout << "Duration : " << current_song.duration << "Sec. = " << current_song.duration / 60 << "Min." << current_song.duration % 60 << "Sec." << endl;
                cout << "Rating : "; // << current_song.rating << " : ";
                for(int i = 0; i < 5; i++){
                    if(i < current_song.rating){
                        cout << "+ ";
                    }else{
                        cout << "- ";
                    }
                }
                cout << endl << endl;
            }
            fclose(file);
        }
}

/*int search(){

}*/

